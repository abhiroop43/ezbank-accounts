-- SELECT 'CREATE DATABASE ezbank_accounts'
--     WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'ezbank_accounts')\gexec;

create table if not exists customer
(
    customer_id   bigserial
        primary key,
    created_at    timestamp(6),
    created_by    varchar(255),
    updated_at    timestamp(6),
    updated_by    varchar(255),
    email         varchar(255),
    mobile_number varchar(255),
    name          varchar(255)
);


create table if not exists accounts
(
    account_number   bigint not null
        primary key,
    created_at       timestamp(6),
    created_by       varchar(255),
    updated_at       timestamp(6),
    updated_by       varchar(255),
    account_type     varchar(255),
    branch_address   varchar(255),
    communication_sw boolean,
    customer_id      bigint
);