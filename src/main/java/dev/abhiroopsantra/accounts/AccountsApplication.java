package dev.abhiroopsantra.accounts;

import dev.abhiroopsantra.accounts.dto.AccountsContactInfoDto;
import io.swagger.v3.oas.annotations.ExternalDocumentation;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableFeignClients
@EnableConfigurationProperties(value = {AccountsContactInfoDto.class})
@EnableJpaAuditing(auditorAwareRef = "auditAwareImpl")
@OpenAPIDefinition(
    info = @Info(
        title = "Accounts microservice REST API Documentation",
        description = "EZBank Accounts REST API documentation", version = "v1", contact = @Contact(
        name = "Abhiroop Santra", email = "abhiroop.santra@gmail.com", url = "https://abhiroopsantra.dev"
    ), license = @License(
        name = "Apache 2.0", url = "https://www.apache.org/licenses/LICENSE-2.0"
    )
    ), externalDocs = @ExternalDocumentation(
    description = "EZBank Accounts REST API documentation", url = "https://swagger.io/specification/"
)
)
public class AccountsApplication {

  public static void main(String[] args) {
    SpringApplication.run(AccountsApplication.class, args);
  }

}
