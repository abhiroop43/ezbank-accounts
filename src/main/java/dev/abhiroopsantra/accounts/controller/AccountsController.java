package dev.abhiroopsantra.accounts.controller;

import dev.abhiroopsantra.accounts.constants.AccountConstants;
import dev.abhiroopsantra.accounts.dto.AccountsContactInfoDto;
import dev.abhiroopsantra.accounts.dto.CustomerDto;
import dev.abhiroopsantra.accounts.dto.ErrorResponseDto;
import dev.abhiroopsantra.accounts.dto.ResponseDto;
import dev.abhiroopsantra.accounts.service.IAccountsService;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import io.github.resilience4j.retry.annotation.Retry;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Tag(
    name = "CRUD REST API for Accounts in EZBank",
    description = "CRUD REST API in EZBank to create, update, fetch and delete account details"
)
@RestController
@RequestMapping(path = "/api", produces = {MediaType.APPLICATION_JSON_VALUE})
//@AllArgsConstructor
@Validated
public class AccountsController {

  private static final Logger logger = LoggerFactory.getLogger(AccountsController.class);
  private final IAccountsService accountsService;
  private final Environment environment;
  private final AccountsContactInfoDto accountsContactInfoDto;
  @Value("${build.version}")
  private String buildVersion;

  public AccountsController(
      IAccountsService accountsService, Environment environment,
      AccountsContactInfoDto accountsContactInfoDto
  ) {
    this.accountsService = accountsService;
    this.environment = environment;
    this.accountsContactInfoDto = accountsContactInfoDto;
  }

  @Operation(
      summary = "Create account REST API endpoint",
      description = "REST API endpoint to create new customer and account in EZBank"
  )
  @ApiResponses(
      {
          @ApiResponse(
              responseCode = "201", description = "HTTP Status CREATED"
          ), @ApiResponse(
          responseCode = "400", description = "HTTP Status BAD REQUEST", content = @Content(
          schema = @Schema(implementation = ErrorResponseDto.class)
      )
      ), @ApiResponse(
          responseCode = "500", description = "HTTP Status INTERNAL SERVER ERROR", content = @Content(
          schema = @Schema(implementation = ErrorResponseDto.class)
      )
      )
      }
  )
  @PostMapping("/create")
  public ResponseEntity<ResponseDto> createAccount(@Valid @RequestBody CustomerDto customerDto) {

    accountsService.createAccount(customerDto);

    return ResponseEntity.status(HttpStatus.CREATED)
        .body(new ResponseDto(AccountConstants.STATUS_201, AccountConstants.MESSAGE_201));
  }

  @Operation(
      summary = "Fetch Account details REST API",
      description = "REST API to fetch Customer and Account details based on mobile number"
  )
  @ApiResponse(
      responseCode = "200", description = "HTTP Status OK"
  )
  @GetMapping("/fetch")
  public ResponseEntity<CustomerDto> fetchAccountDetails(
      @RequestParam
      @Pattern(regexp = "(^$|[0-9]{10})", message = "Mobile number must be 10 digits") String mobileNumber
  ) {
    CustomerDto customerDto = accountsService.fetchAccountDetails(mobileNumber);

    return ResponseEntity.status(HttpStatus.OK).body(customerDto);
  }

  @Operation(
      summary = "Update Account details REST API",
      description = "REST API to update Customer and Account details in EZBank"
  )
  @ApiResponses(
      {
          @ApiResponse(
              responseCode = "200", description = "HTTP Status OK"
          ), @ApiResponse(
          responseCode = "304", description = "HTTP Status NOT MODIFIED"
      ), @ApiResponse(
          responseCode = "400", description = "HTTP Status BAD REQUEST", content = @Content(
          schema = @Schema(implementation = ErrorResponseDto.class)
      )
      ), @ApiResponse(
          responseCode = "500", description = "HTTP Status INTERNAL SERVER ERROR", content = @Content(
          schema = @Schema(implementation = ErrorResponseDto.class)
      )
      )
      }
  )
  @PutMapping("/update")
  public ResponseEntity<ResponseDto> updateAccount(@Valid @RequestBody CustomerDto customerDto) {
    boolean isUpdated = accountsService.updateAccount(customerDto);

    if (isUpdated) {
      return ResponseEntity.status(HttpStatus.OK)
          .body(new ResponseDto(AccountConstants.STATUS_200, AccountConstants.MESSAGE_200));
    } else {
      return ResponseEntity.status(HttpStatus.NOT_MODIFIED)
          .body(new ResponseDto(AccountConstants.STATUS_304, AccountConstants.MESSAGE_304));
    }
  }

  @Operation(
      summary = "Delete Account details REST API",
      description = "REST API to delete Customer and Account details based on mobile number"
  )
  @ApiResponses(
      {
          @ApiResponse(
              responseCode = "200", description = "HTTP Status OK"
          ), @ApiResponse(
          responseCode = "304", description = "HTTP Status NOT MODIFIED"
      ), @ApiResponse(
          responseCode = "400", description = "HTTP Status BAD REQUEST", content = @Content(
          schema = @Schema(implementation = ErrorResponseDto.class)
      )
      ), @ApiResponse(
          responseCode = "500", description = "HTTP Status INTERNAL SERVER ERROR", content = @Content(
          schema = @Schema(implementation = ErrorResponseDto.class)
      )
      )
      }
  )
  @DeleteMapping("/delete")
  public ResponseEntity<ResponseDto> deleteAccount(
      @RequestParam
      @Pattern(regexp = "(^$|[0-9]{10})", message = "Mobile number must be 10 digits") String mobileNumber
  ) {
    boolean isDeleted = accountsService.deleteAccount(mobileNumber);

    if (isDeleted) {
      return ResponseEntity.status(HttpStatus.OK)
          .body(new ResponseDto(AccountConstants.STATUS_200, AccountConstants.MESSAGE_200));
    } else {
      return ResponseEntity.status(HttpStatus.NOT_MODIFIED)
          .body(new ResponseDto(AccountConstants.STATUS_304, AccountConstants.MESSAGE_304));
    }
  }

  @Operation(
      summary = "Fetch the build version of the application",
      description = "REST API to fetch the build version of the application"
  )
  @ApiResponses(
      {
          @ApiResponse(
              responseCode = "200", description = "HTTP Status OK"
          ), @ApiResponse(
          responseCode = "500", description = "HTTP Status Internal Server Error", content = @Content(
          schema = @Schema(implementation = ErrorResponseDto.class)
      )
      )
      }
  )
  @Retry(name = "getBuildInfo", fallbackMethod = "getBuildInfoFallBack")
  @GetMapping("/build-info")
  public ResponseEntity<String> getBuildInfo() {
    logger.debug("getBuildInfo() method invoked");
    return ResponseEntity.status(HttpStatus.OK).body(buildVersion);
  }

  // fallback method signature needs to be the same as getBuildInfo
  // fallback method must have input parameter as Throwable
  public ResponseEntity<String> getBuildInfoFallBack(Throwable throwable) {
    logger.debug("getBuildInfoFallback() method invoked");
    return ResponseEntity.status(HttpStatus.OK).body("0.9");
  }

  @Operation(
      summary = "Fetch the java version configured in the server",
      description = "REST API to fetch the java version configured in the server"
  )
  @ApiResponses(
      {
          @ApiResponse(
              responseCode = "200", description = "HTTP Status OK"
          ), @ApiResponse(
          responseCode = "500", description = "HTTP Status Internal Server Error", content = @Content(
          schema = @Schema(implementation = ErrorResponseDto.class)
      )
      )
      }
  )
  @RateLimiter(name = "getJavaVersion", fallbackMethod = "getJavaVersionFallBack")
  @GetMapping("/java-version")
  public ResponseEntity<String> getJavaVersion() {
    return ResponseEntity.status(HttpStatus.OK).body(environment.getProperty("JAVA_HOME"));
  }

  public ResponseEntity<String> getJavaVersionFallBack(Throwable throwable) {
    return ResponseEntity.status(HttpStatus.OK).body("JAVA 21");
  }

  @Operation(
      summary = "Get Contact Info", description = "Contact Info Details in case of any issue"
  )
  @ApiResponses(
      {
          @ApiResponse(
              responseCode = "200", description = "HTTP Status OK"
          ), @ApiResponse(
          responseCode = "500", description = "HTTP Status Internal Server Error", content = @Content(
          schema = @Schema(implementation = ErrorResponseDto.class)
      )
      )
      }
  )
  @GetMapping("/contact-info")
  public ResponseEntity<AccountsContactInfoDto> getContactInfo() {
    return ResponseEntity.status(HttpStatus.OK).body(accountsContactInfoDto);
  }
}
