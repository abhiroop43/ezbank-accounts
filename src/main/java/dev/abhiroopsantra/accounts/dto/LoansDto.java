package dev.abhiroopsantra.accounts.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.Data;

@Data public class LoansDto {
    @NotEmpty(message = "Mobile number cannot be null or empty")
    @Pattern(regexp = "(^$|[0-9]{10})", message = "Mobile number must be 10 digits")
    private String mobileNumber;

    @NotEmpty(message = "Loan Number cannot be null or empty")
    @Pattern(regexp = "(^$|[0-9]{12})", message = "Loan number must be 10 digits")
    private String loanNumber;

    @NotEmpty(message = "Loan Type cannot be null or empty")
    private String loanType;

    @Positive(message = "Total loan amount must be greater than 0")
    private Float totalLoan;

    @PositiveOrZero(message = "Loan amount paid must be greater than or equal to 0")
    private Float amountPaid;

    @PositiveOrZero(message = "Loan outstanding amount must be greater than or equal to 0")
    private Float outstandingAmount;
}
