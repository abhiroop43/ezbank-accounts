package dev.abhiroopsantra.accounts.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.Data;

@Data public class CardsDto {
    @NotEmpty(message = "Mobile number cannot be null or empty")
    @Pattern(regexp = "(^$|[0-9]{10})", message = "Mobile number must be 10 digits")
    private String mobileNumber;

    @NotEmpty(message = "Card Number cannot be null or empty")
    @Pattern(regexp = "(^$|[0-9]{12})", message = "Card number must be 12 digits")
    private String cardNumber;

    @NotEmpty(message = "Card Type cannot be null or empty")
    private String cardType;

    @Positive(message = "Card total limit must be greater than 0")
    private float totalLimit;

    @PositiveOrZero(message = "Card amount used must be greater than or equal to 0")
    private float amountUsed;

    @PositiveOrZero(message = "Card available amount must be greater than or equal to 0")
    private float availableAmount;
}
