package dev.abhiroopsantra.accounts.dto;

/**
 * DTO class for Accounts Message communication
 *
 * @param accountNumber
 * @param name
 * @param email
 * @param mobileNumber
 */
public record AccountsMsgDto(Long accountNumber, String name, String email, String mobileNumber) {

}
