package dev.abhiroopsantra.accounts.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

@Data @Schema(
        name = "Accounts", description = "Schema to hold Account information"
) public class AccountsDto {

    @Schema(
            description = "Account number in EZ Bank", example = "1234567890"
    )
    @NotEmpty(message = "Account number cannot be null or empty")
    @Pattern(regexp = "($|[0-9]{10})", message = "Account number must be 10 digits")
    private Long accountNumber;

    @Schema(
            description = "Account type in EZ Bank", example = "Savings"
    )
    @NotEmpty(message = "Account Type cannot be null or empty")
    private String accountType;

    @Schema(
            description = "Branch address in EZ Bank", example = "8 Brass Citadel"
    )
    @NotEmpty(message = "Branch address cannot be null or empty")
    private String branchAddress;
}
