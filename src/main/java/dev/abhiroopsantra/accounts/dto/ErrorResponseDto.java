package dev.abhiroopsantra.accounts.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Schema(
        name = "ErrorResponse", description = "Schema to hold the error response data"
) @Data @AllArgsConstructor public class ErrorResponseDto {

    @Schema(
            description = "API path invoked by the client"
    )
    private String apiPath;

    @Schema(
            description = "Error code representing the error happened"
    )
    private HttpStatus httpStatus;

    @Schema(
            description = "A detailed message of the error"
    )
    private String errorMessage;

    @Schema(
            description = "The date and time of the error occurrence"
    )
    private LocalDateTime errorTime;
}
