package dev.abhiroopsantra.accounts.service;

import dev.abhiroopsantra.accounts.dto.CustomerDetailsDto;

public interface ICustomerService {

    /**
     * Fetch customer details based on mobile number
     *
     * @param mobileNumber  Mobile number of the customer
     * @param correlationId
     * @return CustomerDetailsDto
     */
    CustomerDetailsDto fetchCustomerDetails(String mobileNumber, String correlationId);
}
