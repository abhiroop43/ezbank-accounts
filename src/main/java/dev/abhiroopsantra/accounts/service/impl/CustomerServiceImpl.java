package dev.abhiroopsantra.accounts.service.impl;

import dev.abhiroopsantra.accounts.dto.*;
import dev.abhiroopsantra.accounts.entity.Accounts;
import dev.abhiroopsantra.accounts.entity.Customer;
import dev.abhiroopsantra.accounts.exception.ResourceNotFoundException;
import dev.abhiroopsantra.accounts.mapper.AccountsMapper;
import dev.abhiroopsantra.accounts.mapper.CustomerMapper;
import dev.abhiroopsantra.accounts.repository.AccountsRepository;
import dev.abhiroopsantra.accounts.repository.CustomerRepository;
import dev.abhiroopsantra.accounts.service.ICustomerService;
import dev.abhiroopsantra.accounts.service.client.CardsFeignClient;
import dev.abhiroopsantra.accounts.service.client.LoansFeignClient;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service @AllArgsConstructor public class CustomerServiceImpl implements ICustomerService {

    private AccountsRepository accountsRepository;
    private CustomerRepository customerRepository;
    private CardsFeignClient   cardsFeignClient;
    private LoansFeignClient   loansFeignClient;

    /**
     * Fetch customer details based on mobile number
     *
     * @param mobileNumber  Mobile number of the customer
     * @param correlationId Correlation ID
     * @return CustomerDetailsDto
     */
    @Override public CustomerDetailsDto fetchCustomerDetails(String mobileNumber, String correlationId) {
        Customer customer = customerRepository.findByMobileNumber(mobileNumber).orElseThrow(
                () -> new ResourceNotFoundException("Customer", "mobileNumber", mobileNumber));

        Accounts accounts = accountsRepository.findByCustomerId(customer.getCustomerId()).orElseThrow(
                () -> new ResourceNotFoundException("Accounts", "customerId", customer.getCustomerId().toString()));

        // map customer details
        CustomerDetailsDto customerDetailsDto = CustomerMapper.mapToCustomerDetailsDto(
                customer, new CustomerDetailsDto());

        // map accounts details
        customerDetailsDto.setAccountsDto(AccountsMapper.mapToAccountsDto(accounts, new AccountsDto()));

        // fetch loan details
        ResponseEntity<LoansDto> loansDtoResponseEntity = loansFeignClient.fetchLoanDetails(
                correlationId, mobileNumber);

        if (loansDtoResponseEntity != null) {
            // map loan details
            customerDetailsDto.setLoansDto(loansDtoResponseEntity.getBody());
        }

        // fetch card details
        ResponseEntity<CardsDto> cardsDtoResponseEntity = cardsFeignClient.fetchCardDetails(
                correlationId, mobileNumber);

        if (cardsDtoResponseEntity != null) {
            // map card details
            customerDetailsDto.setCardsDto(cardsDtoResponseEntity.getBody());
        }

        return customerDetailsDto;
    }
}
