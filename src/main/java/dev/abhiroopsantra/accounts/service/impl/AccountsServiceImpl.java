package dev.abhiroopsantra.accounts.service.impl;

import dev.abhiroopsantra.accounts.constants.AccountConstants;
import dev.abhiroopsantra.accounts.dto.AccountsDto;
import dev.abhiroopsantra.accounts.dto.AccountsMsgDto;
import dev.abhiroopsantra.accounts.dto.CustomerDto;
import dev.abhiroopsantra.accounts.entity.Accounts;
import dev.abhiroopsantra.accounts.entity.Customer;
import dev.abhiroopsantra.accounts.exception.CustomerAlreadyExistsException;
import dev.abhiroopsantra.accounts.exception.ResourceNotFoundException;
import dev.abhiroopsantra.accounts.mapper.AccountsMapper;
import dev.abhiroopsantra.accounts.mapper.CustomerMapper;
import dev.abhiroopsantra.accounts.repository.AccountsRepository;
import dev.abhiroopsantra.accounts.repository.CustomerRepository;
import dev.abhiroopsantra.accounts.service.IAccountsService;
import java.util.Optional;
import java.util.Random;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AccountsServiceImpl implements IAccountsService {

  private static final Logger log = LoggerFactory.getLogger(AccountsServiceImpl.class);
  private final StreamBridge streamBridge;
  private AccountsRepository accountsRepository;
  private CustomerRepository customerRepository;

  /**
   * Create a new account.
   *
   * @param customerDto the customer dto object
   */
  @Override
  public void createAccount(CustomerDto customerDto) {
    Customer customer = CustomerMapper.mapToCustomer(customerDto, new Customer());

    Optional<Customer> optionalCustomer = customerRepository.findByMobileNumber(
        customerDto.getMobileNumber());

    if (optionalCustomer.isPresent()) {
      throw new CustomerAlreadyExistsException(
          "Customer already registered with the given mobile number: %s".formatted(
              customerDto.getMobileNumber()));
    }

    Customer savedCustomer = customerRepository.save(customer);
    Accounts savedAccount = accountsRepository.save(createNewAccount(savedCustomer));
    sendCommunication(savedAccount, customer);

  }

  /**
   * Create a new account.
   *
   * @param customer the customer object
   * @return the new accounts object
   */
  private Accounts createNewAccount(Customer customer) {
    Accounts newAccount = new Accounts();
    newAccount.setCustomerId(customer.getCustomerId());

    long randomAccountNumber = 1000000000L + new Random().nextInt(900000000);

    newAccount.setAccountNumber(randomAccountNumber);
    newAccount.setAccountType(AccountConstants.SAVINGS);
    newAccount.setBranchAddress(AccountConstants.ADDRESS);

    return newAccount;
  }


  /**
   * Fetch account details.
   *
   * @param mobileNumber the mobile number
   * @return the customer dto
   */
  @Override
  public CustomerDto fetchAccountDetails(String mobileNumber) {
    Customer customer = customerRepository.findByMobileNumber(mobileNumber).orElseThrow(
        () -> new ResourceNotFoundException("Customer", "mobileNumber", mobileNumber));

    Accounts accounts = accountsRepository.findByCustomerId(customer.getCustomerId()).orElseThrow(
        () -> new ResourceNotFoundException("Accounts", "customerId",
            customer.getCustomerId().toString()));

    CustomerDto customerDto = CustomerMapper.mapToCustomerDto(customer, new CustomerDto());
    customerDto.setAccountsDto(AccountsMapper.mapToAccountsDto(accounts, new AccountsDto()));
    return customerDto;
  }

  /**
   * Update account.
   *
   * @param customerDto the customer dto
   * @return the boolean
   */
  @Override
  public boolean updateAccount(CustomerDto customerDto) {
    AccountsDto accountsDto = customerDto.getAccountsDto();

    if (accountsDto == null) {
      return false;
    }
    Accounts accounts = accountsRepository.findById(accountsDto.getAccountNumber()).orElseThrow(
        () -> new ResourceNotFoundException("Accounts", "accountNumber",
            accountsDto.getAccountNumber().toString()
        ));

    AccountsMapper.mapToAccounts(accountsDto, accounts);

    accounts = accountsRepository.save(accounts);

    Long customerId = accounts.getCustomerId();
    Customer customer = customerRepository.findById(customerId).orElseThrow(
        () -> new ResourceNotFoundException("Customer", "customerId", customerId.toString()));

    CustomerMapper.mapToCustomer(customerDto, customer);

    customerRepository.save(customer);

    return true;
  }

  /**
   * Delete account.
   *
   * @param mobileNumber the mobile number
   * @return the boolean
   */
  @Override
  public boolean deleteAccount(String mobileNumber) {
    Customer customer = customerRepository.findByMobileNumber(mobileNumber).orElseThrow(
        () -> new ResourceNotFoundException("Customer", "mobileNumber", mobileNumber));

    accountsRepository.deleteByCustomerId(customer.getCustomerId());
    customerRepository.delete(customer);

    return true;
  }

  /**
   * Update communication status.
   *
   * @param accountNumber the account number
   * @return the boolean
   */
  @Override
  public boolean updateCommunicationStatus(Long accountNumber) {
    boolean isUpdated = false;

    if (accountNumber != null) {
      Accounts accounts = accountsRepository.findById(accountNumber).orElseThrow(
          () -> new ResourceNotFoundException("Accounts", "accountNumber",
              accountNumber.toString()));

      accounts.setCommunicationSw(true);
      accountsRepository.save(accounts);

      isUpdated = true;
    }

    return isUpdated;
  }

  private void sendCommunication(Accounts account, Customer customer) {
    var accountsMsgDto = new AccountsMsgDto(account.getAccountNumber(), customer.getName(),
        customer.getEmail(), customer.getMobileNumber());
    log.info("Sending communication request for the details: {}", accountsMsgDto);
    var result = streamBridge.send("sendCommunication-out-0", accountsMsgDto);
    log.info("Communication request process success: {}", result);
  }
}
