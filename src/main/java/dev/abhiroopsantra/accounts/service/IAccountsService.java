package dev.abhiroopsantra.accounts.service;

import dev.abhiroopsantra.accounts.dto.CustomerDto;

public interface IAccountsService {

  /**
   * Create a new account.
   *
   * @param customerDto the customer dto object
   */
  void createAccount(CustomerDto customerDto);

  /**
   * Fetch account details.
   *
   * @param mobileNumber the mobile number
   * @return the customer dto
   */
  CustomerDto fetchAccountDetails(String mobileNumber);

  /**
   * Update account details.
   *
   * @param customerDto the customer dto object
   * @return the boolean
   */
  boolean updateAccount(CustomerDto customerDto);

  /**
   * Delete account details.
   *
   * @param mobileNumber the mobile number
   * @return the boolean
   */
  boolean deleteAccount(String mobileNumber);

  /**
   * Update communication status.
   *
   * @param accountNumber the account number
   * @return the boolean
   */
  boolean updateCommunicationStatus(Long accountNumber);
}
